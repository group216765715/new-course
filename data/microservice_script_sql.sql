create database courseService;
use courseService;

create table Course(
	course_id int auto_increment primary key not null,
    course_name varchar(255) not null,
    course_lesson int not null,
    start_date date,
    end_date datetime
);
alter table Course add course_image varchar(255);
alter table Course add course_types varchar(255);
alter table Course add course_categories varchar(255);
ALTER TABLE Course CHANGE course_image image longblob;


-----
create database userService;
use userService;

create table UserAccount(
	user_id int auto_increment primary key not null,
    username varchar(255) not null,
    email varchar(255) not null,
    password varchar(255) not null,
    password_reset_token varchar(255),
    active bit
);

CREATE TABLE `UserRole` (
  `user_id` INTEGER NOT NULL,
  `role_id` INTEGER NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`)
);

CREATE TABLE `Role` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(50) NOT NULL UNIQUE,
  PRIMARY KEY (`id`)
);

-----
create database wishlistService;
use wishlistService;

-----
create database evaluateService;
use evaluateService;

-----
create database cartService;
use cartService;

