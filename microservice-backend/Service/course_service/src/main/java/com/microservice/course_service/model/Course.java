package com.microservice.course_service.model;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity(name = "Course")
public class Course {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int course_id;
	
	@Column(name = "course_name")
	private String course_name;
	
	@Column(name = "course_lesson")
	private int course_lesson;
	
	@Column(name = "image")
	private String image;
	
	@Column(name = "course_types")
	private String course_types;
	
	@Column(name = "course_categories")
	private String course_categories;
	
	@Column(name = "start_date")
	private Date start_date;
	
	@Column(name = "end_date")
	private Date end_date;

	public int getCourse_id() {
		return course_id;
	}

	public void setCourse_id(int course_id) {
		this.course_id = course_id;
	}

	public String getCourse_name() {
		return course_name;
	}

	public void setCourse_name(String course_name) {
		this.course_name = course_name;
	}

	public int getCourse_lesson() {
		return course_lesson;
	}

	public void setCourse_lesson(int course_lesson) {
		this.course_lesson = course_lesson;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getCourse_types() {
		return course_types;
	}

	public void setCourse_types(String course_types) {
		this.course_types = course_types;
	}

	public String getCourse_categories() {
		return course_categories;
	}

	public void setCourse_categories(String course_categories) {
		this.course_categories = course_categories;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public Course() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Course(int course_id, String course_name, int course_lesson, String image, String course_types,
			String course_categories, Date start_date, Date end_date) {
		super();
		this.course_id = course_id;
		this.course_name = course_name;
		this.course_lesson = course_lesson;
		this.image = image;
		this.course_types = course_types;
		this.course_categories = course_categories;
		this.start_date = start_date;
		this.end_date = end_date;
	}

	
	
	
	
}
