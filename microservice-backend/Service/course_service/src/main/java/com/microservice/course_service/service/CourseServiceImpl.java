package com.microservice.course_service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import com.microservice.course_service.model.Course;
import com.microservice.course_service.repository.CourseRepository;

@Service
public class CourseServiceImpl implements CourseService {
	@Autowired
	private CourseRepository courseRepository;

	@Override
	public List<Course> getAllCourse() {
		// TODO Auto-generated method stub
		return courseRepository.findAll();
	}

	@Override
	public Course getCourseById(int course_id) {
		// TODO Auto-generated method stub
		return courseRepository.
				findById(course_id)
				.orElseThrow(() -> new RuntimeException("Not found course had id: " + course_id));
	}

	@Override
	public Course createCourse(Course course) {
		// TODO Auto-generated method stub
		return courseRepository.save(course);
	}

	@Override
	public Course updateCourse(Integer course_id, Course courseUpdate) {
		// TODO Auto-generated method stub
		Course course = courseRepository.findById(course_id)
				.orElseThrow(() -> new RuntimeException("Not found customer has id: " + course_id));
		// update new course
		course.setCourse_name(courseUpdate.getCourse_name());
		course.setImage(courseUpdate.getImage());
		course.setCourse_lesson(courseUpdate.getCourse_lesson());
		course.setCourse_types(courseUpdate.getCourse_types());
		course.setCourse_categories(courseUpdate.getCourse_categories());
		course.setStart_date(courseUpdate.getStart_date());
		course.setEnd_date(courseUpdate.getEnd_date());
		
		return courseRepository.save(course);
	}

	@Override
	public void deleteCourse(int course_id) {
		// TODO Auto-generated method stub
		
	}
	
}
