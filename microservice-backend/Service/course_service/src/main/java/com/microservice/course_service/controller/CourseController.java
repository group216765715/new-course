package com.microservice.course_service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.microservice.course_service.model.Course;
import com.microservice.course_service.service.CourseService;

@RestController
@RequestMapping("/course")
@CrossOrigin(origins = "http://localhost:3000")
public class CourseController {

	@Autowired
	private CourseService courseService;
	
	// get all courses
	@GetMapping("")
	public ResponseEntity<List<Course>> getAllCourses(){
		List<Course> course = courseService.getAllCourse();
		return ResponseEntity.ok(course);
	}
	
	// get course by id
	@GetMapping("/{id}")
	public ResponseEntity<Course> findCourseById(@PathVariable Integer id){
		Course course = courseService.getCourseById(id);
		return ResponseEntity.ok(course);
	}
	
	// create course
	@PostMapping("")
	public ResponseEntity<Course> createCourse(@RequestBody Course course){
		Course createCourse = courseService.createCourse(course);
		return ResponseEntity.ok(createCourse);
	}
	
	// update course by id
	@PutMapping("/{id}")
	public ResponseEntity<Course> updateCourseById(@PathVariable Integer id, 
			@RequestBody Course course){
		Course updateCourse = courseService.updateCourse(id, course);
		return ResponseEntity.ok(updateCourse);
	}
	
	// delete course by id
}
