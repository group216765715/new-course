package com.microservice.user_service.service;
import java.util.List;

import org.springframework.stereotype.Service;

import com.microservice.user_service.model.User;

@Service
public interface UserService {
	
	List<User> getAllUsers();
	
	User getUserById(Integer user_id);
	
	User createNewUser(User user);
	
	User updateUser(Integer user_id, User user);
	
	void deleteUser(Integer user_id);
}
