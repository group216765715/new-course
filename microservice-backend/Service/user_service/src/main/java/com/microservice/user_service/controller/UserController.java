package com.microservice.user_service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.microservice.user_service.model.User;
import com.microservice.user_service.service.UserService;



@RestController
@RequestMapping("/user")
public class UserController {
	
	@GetMapping("/home")
	public String home() {
		return "home";
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> signin(){
		return new ResponseEntity<>("sign in", HttpStatus.OK);
	}
	
	@Autowired
	private UserService userService;
//
//	@GetMapping("/home")
//	public String home() {
//		return "home";
//	}
//	
//	@GetMapping("/login")
//	public String helloLogin() {
//		return "login";
//	}
//	
	@GetMapping("")
	public ResponseEntity<List<User>> getAllUsers(){
		List<User> users = userService.getAllUsers();
		return ResponseEntity.ok(users);
	}
//	
//	@GetMapping("/user/{id}")
//	public ResponseEntity<User> getUserById(@PathVariable Integer id){
//		User user = userService.getUserById(id);
//		return ResponseEntity.ok(user);
//	}
//	
	@GetMapping("/signup")
	public String createNewUser() {
		return "Created!";
	}
	@PostMapping("/signup")
	public ResponseEntity<User> createUser(@RequestBody User user){
		User createUser = userService.createNewUser(user);
		return ResponseEntity.ok(createUser);
	}
}
