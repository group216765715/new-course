package com.microservice.user_service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.microservice.user_service.model.User;
import com.microservice.user_service.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{
	@Autowired
	private UserRepository userRepository;

	@Override
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}
	
	@Override
	public User getUserById(Integer user_id) {
		// TODO Auto-generated method stub
		return userRepository.findById(user_id)
				.orElseThrow(() -> new RuntimeException("Not found user had id: " + user_id));
	}

	@Override
	public User createNewUser(User user) {
		// TODO Auto-generated method stub
		return userRepository.save(user);
	}

	@Override
	public User updateUser(Integer user_id, User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteUser(Integer user_id) {
		// TODO Auto-generated method stub
		
	}

	

}
