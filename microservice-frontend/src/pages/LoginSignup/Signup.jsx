import React from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { Link } from 'react-router-dom';
import Container from 'react-bootstrap/esm/Container';

export const Signup = () => {
    return (
        <div className="signup my-4">
            <Container>
                <Form className='form-control  p-4' action='/http://localhost:8088/course/signup' method='post'>
                    <Form.Group className='mb-3'>
                        <Form.Label className='h1'>Sign Up</Form.Label>
                    </Form.Group>

                    <Form.Group className='mb-3'>
                        <Form.Label className='mb-3'>Full Name</Form.Label>
                        <Form.Control type='text' placeholder='Enter Your Name' />
                    </Form.Group>

                    <Form.Group className='mb-3'>
                        <Form.Label className='mb-3'>Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" />
                    </Form.Group>

                    <Form.Group className='mb-4'>
                        <Form.Label className='mb-3'>Password</Form.Label>
                        <Form.Control type="password" placeholder="Enter Password" />
                    </Form.Group>

                    <Form.Group className='mb-3'>
                        <Button className='w-100' variant="primary" type="submit">
                            Sign Up
                        </Button>
                    </Form.Group>

                    <Form.Group>
                        <span>Already have an account? <Link to="/login">Log In</Link></span>
                    </Form.Group>
                </Form>
            </Container>
        </div>
    )
}
