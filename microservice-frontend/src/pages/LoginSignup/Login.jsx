import Button from 'react-bootstrap/Button'
import React, { useEffect, useState } from 'react'
import Form from 'react-bootstrap/Form'
import './Form.css'
import { Link } from 'react-router-dom';
import Container from 'react-bootstrap/esm/Container';

export const Login = () => {
    // const [email, setEmail] = useState([]);
    // useEffect(() => {
    //     fetch("http://localhost:8081/user")
    //         .then(res => res.json())
    //         .then( email => {
    //             setEmail(email)
    //         })
    // }, [])
    return (
        <div className="login my-4">
            <Container>
                <Form className='form-control w-100 p-4' action='/http://localhost:8088/course/login' method='post'>
                    <Form.Group className='mb-3'>
                        <Form.Label className='h1'>Log In</Form.Label>
                    </Form.Group>

                    <Form.Group className='mb-3'>
                        <Form.Label className='mb-3'>Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" />
                    </Form.Group>

                    <Form.Group className='mb-4'>
                        <Form.Label className='mb-3'>Password</Form.Label>
                        <Form.Control type="password" placeholder="Enter Password" />
                    </Form.Group>

                    <Form.Group className='mb-3'>
                        <Button className='w-100' variant="primary" type="submit">
                            Log In
                        </Button>
                    </Form.Group>

                    <Form.Group>
                        <span>or <Link to="/">Forgot Password</Link></span>
                        <hr />
                        <p>Don't have an account? <Link to="/signup">Sign Up</Link></p>
                    </Form.Group>
                </Form>
            </Container>
        </div>
    )
}
