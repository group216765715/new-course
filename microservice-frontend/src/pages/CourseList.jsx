import React, { useEffect, useState } from 'react'

import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const CourseList = () => {
    const [courses, setCourse] = useState([]);
    useEffect(() => {
        fetch('http://localhost:8088/course')
            .then(res => res.json())
            .then(courses => {
                setCourse(courses);
            });
    }, [])

    return (
        <div className='course_list p-4'>
            <div className="search">
                <input type="text" />
            </div>
            <h3 className='my-3'>My Learning</h3>
            <Table striped bordered hover className='text-center align-items-center'>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th className='w-25'>Name</th>
                        <th>Immage</th>
                        <th>Lesson</th>
                        <th>Type</th>
                        <th>Categories</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Active</th>
                    </tr>
                </thead>

                <tbody>
                    {courses.map((course, course_id) => (
                        <tr key={course_id}>
                            <td>{course.course_id}</td>
                            <td>{course.course_name}</td>
                            <td>{course.course_image}</td>
                            <td>{course.course_lesson}</td>
                            <td>{course.course_types}</td>
                            <td>{course.course_categories}</td>
                            <td>{course.start_date}</td>
                            <td>{course.end_date}</td>
                            <td>
                                <Button variant="secondary" className='me-2'>
                                <FontAwesomeIcon icon="fa-regular fa-eye" />
                                </Button>
                                <Button variant='primary' className='me-2'>Edit</Button>
                                <Button variant="danger">X</Button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </Table>

            <div className="pagnigation">

            </div>
        </div>
    )
}

// ---------------
// <tr >
                        //     <td>
                        //         <Course key={course_id} 
                        //                 course_name={course.course_name}
                        //                 course_image={course.course_image}
                        //                 course_lesson={course.course_lesson}
                        //                 start_date={course.start_date}
                        //                 end_date={course.end_date}
                        //             />
                        //     </td>
                        // </tr>// <tr >
                        //     <td>
                        //         <Course key={course_id} 
                        //                 course_name={course.course_name}
                        //                 course_image={course.course_image}
                        //                 course_lesson={course.course_lesson}
                        //                 start_date={course.start_date}
                        //                 end_date={course.end_date}
                        //             />
                        //     </td>
                        // </tr>