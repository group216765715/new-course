import React from 'react'

export const Admin = () => {
    return (
        <div className='admin_pages d-flex '>
            <div className="sidebar w-25 bg-primary h-100 p-3">
                <div className="logo">
                    <h2 className='me-3'>Udemy</h2>
                </div>
            </div>
            <div className="tab-content w-75 bg-secondary">
                admin
            </div>
        </div>
    )
}
