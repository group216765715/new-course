import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { CourseList } from './pages/CourseList';
import { NavBar } from './components/Navbar/NavBar';
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { Login } from './pages/LoginSignup/Login';
import { Signup } from './pages/LoginSignup/Signup';
import { Cart } from './components/Cart/Cart';
import { Home } from './components/Home/Home';
import { Footer } from './components/Footer/Footer'
import { Learning } from './components/Learning/Learning';
import { Admin } from './pages/admin/Admin';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <NavBar />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/my-course' />
            <Route path='/learning' element={<Learning />} />
          <Route path='/lists' />
          <Route path='/wishlist' />
          <Route path='/archived' />
          <Route path='/learning-tools' />
          <Route />
          <Route path='/login' element={<Login />} />
          <Route path='/signup' element={<Signup />} />
          <Route path='/cart' element={<Cart />} />
          
        </Routes>
        <Footer />
        <Admin/>
      </BrowserRouter>

    </div>
  );
}

export default App;

{/* <Route path='/my-course'/>
  <Route path='/lists'/>
  <Route path='/wishlist'/>
  <Route path='/archived'/>
  <Route path='/learning-tools'/> */}