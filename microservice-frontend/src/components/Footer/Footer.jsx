import React from 'react'
import Container from 'react-bootstrap/esm/Container'
import Button from 'react-bootstrap/Button'

export const Footer = () => {
  return (
    <div className='footer bg-secondary'>
        <div className="footer-head border">
          <Container className='d-flex justify-content-between align-items-center py-3'>
            <h4>Top companies choose Udemy Business to build in-demand career skills.</h4>
            <div className="img">
              <img width={"80px"} className='me-2' src={"https://s.udemycdn.com/partner-logos/v4/nasdaq-light.svg"} alt="" />
              <img width={"80px"} className='me-2' src={"https://s.udemycdn.com/partner-logos/v4/volkswagen-light.svg"} alt="" />
              <img width={"80px"} className='me-2' src={"https://s.udemycdn.com/partner-logos/v4/box-light.svg"} alt="" />
              <img width={"80px"} className='me-2' src={"https://s.udemycdn.com/partner-logos/v4/netapp-light.svg"} alt="" />
              <img width={"80px"} className='me-2' src={"https://s.udemycdn.com/partner-logos/v4/eventbrite-light.svg"} alt="" />
            </div>
          </Container>
        </div>
        <div className="footer-body py-4">
          <Container className='d-flex justify-content-between align-items-center'>
            <div className="footer-instruction"></div>
            <div className="footer-language">
              <Button>English</Button>
            </div>
          </Container>
        </div>
    </div>
  )
}
