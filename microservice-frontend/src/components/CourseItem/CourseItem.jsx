import React from 'react'
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button'

export const CourseItem = (props) => {
    return (
        <div className='course_item'>


            <img src={props.course_image} width={"30px"} alt="" />
            <p style={{background: "blue"}}>{props.course_name}</p>
            <p>{props.course_image}</p>
            <p>{props.course_types}</p>
            <p>{props.course_lesson}</p>
            <p>{props.start_date}</p>
            <p>{props.end_date}</p>
        </div>
    )
}
