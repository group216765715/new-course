import React, { useEffect, useState } from 'react'
import Tab from 'react-bootstrap/Tab'
import Tabs from 'react-bootstrap/Tabs'
import Container from 'react-bootstrap/esm/Container'
import Button from 'react-bootstrap/Button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { CourseItem } from '../CourseItem/CourseItem'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

export const Learning = () => {
    const [courses, setCourses] = useState([])
    useEffect(() => {
        fetch('http://localhost:8088/course')
            .then(res => res.json())
            .then(courses => {
                setCourses(courses)
            })
    }, [])
    console.log(courses)
    return (
        <div className='learning my-5 py-3 bg-dark'>
            <div className="learning-tab  text-light">
                <Container>
                    <h1 className='mb-5'>My Learning</h1>
                    <Tabs
                        defaultActiveKey="all-course"
                        id="uncontrolled-tab-example"
                        className="mb-3"
                    >
                        <Tab className='py-4' eventKey="all-course" title="All Course">
                            {courses.map((courses, course_id) => {
                                return <CourseItem key={course_id}
                                    course_name={courses.course_name}
                                    course_types={courses.course_types}
                                />
                            })}
                        </Tab>
                        <Tab className='py-4' eventKey="mylists" title="My Lists">
                            Tab content for My Lists
                        </Tab>
                        <Tab className='py-4' eventKey="wishlist" title="Wishlist">
                            Tab content for Wishlist
                        </Tab>
                        <Tab className='py-4' eventKey="archived" title="Archived">
                            Tab content for Archived
                        </Tab>
                        <Tab className='py-4' eventKey="learning-tools" title="Learning Tools">
                            Tab content for Learning Tools
                        </Tab>

                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                            </ul>
                        </nav>
                    </Tabs>
                </Container>
            </div>
            <div className="learning-content">

            </div>
        </div>
    )
}
