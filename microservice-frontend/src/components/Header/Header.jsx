import React from 'react'

export const Header = () => {
  return (
    <div className='header'>
        <div className="header-logo">
            <img src="" alt="" />
        </div>
        <div className="header-category"></div>
        <div className="header-search"></div>
        <div className="header-"></div>
        <div className="header-"></div>
        <div className="header-user"></div>
        
    </div>
  )
}
