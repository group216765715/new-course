import React from 'react'
import { Link, NavLink } from 'react-router-dom';
import Form from 'react-bootstrap/Form'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import NavDropdown from 'react-bootstrap/NavDropdown'
import Button from 'react-bootstrap/Button'
import './NavBar.css'
import { Home } from '../Home/Home';


export const NavBar = () => {
    return (
        <div>
            <Navbar expand="lg" className="bg- px-3 py-3 border-bottom shadow">
                <h2 className='me-3'>Udemy</h2>
                <NavDropdown title="Categories" id="basic-nav-dropdown" >
                    <NavDropdown.Item href="#action/3.1">IT & Software</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.2">Design action</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.3">Marketing</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.4">Music</NavDropdown.Item>
                </NavDropdown>
                <Form className='w-100'>
                    <Form.Control formMethod='post'
                        formAction='http://localhost:8088/course'
                        id='search'
                        className='mx-5 px-3'
                        variant="outline-warning"
                        type='text' >

                    </Form.Control>
                </Form>

                <Container className='w-50'>
                    <div className="instruction">
                        <NavLink to="/learning" className="text-light me-3">
                            <Button>My learning</Button>
                        </NavLink>

                        <NavLink to="/wishlist" className="text-light me-3">
                            <Button>Wishlist</Button>
                        </NavLink>

                        <NavLink to="/cart" className="text-light">
                            <Button>Cart</Button>
                        </NavLink>
                    </div>
                    <div className='user'>
                        <NavLink to="/login" className="text-light bg-primary me-2">
                            <Button>Sign In</Button>
                        </NavLink>
                        <NavLink to="/signup" className="text-light bg-primary">
                            <Button>Sign Up</Button>
                        </NavLink>
                    </div>
                    
                </Container>

            </Navbar>
        </div>
    )
}
