import React from 'react'
import './Cart.css'
import Button from 'react-bootstrap/Button'


export const Cart = () => {
    return (
        <div className='cart p-4 d-flex justify-content-between align-items-center'>
            <div className="cart_tem_left w-50">
                <h1>Shopping Cart</h1>
                .<div className="cart_item"></div>
            </div>
            <div className="cart_tem_right w-25">
                <p>Total</p>
                <h4 className='price'></h4>
                <span className='old_price'></span>
                <span className='sales_off'></span>
                <Button>Checkout</Button>
            </div>

        </div>
    )
}
